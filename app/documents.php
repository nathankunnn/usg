<?
namespace App;

use Illuminate\Database\Eloquent\Model;
class Documents extends Model
{
   public $fillable = ['name','details'];
}