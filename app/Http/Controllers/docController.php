<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\documents;

class FileController extends Controller 
{
    public function importExportExcelORCSV()
    {
        return view('file_import_export');
    }

    public function importFileIntoDB(Request $request)
    {
        if($request->hasFile('sample_file')){
            $path = $request->file('sample_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value)
                {
                    $arr[] = ['name' => $value->name, 'desc' => $value->details];
                }
                if(!empty($arr)){
                    \DB::table('products')->insert($arr);
                    dd('Insert Record successfully.');
                }
            }
        }
        dd('Request data does not have any files to import.');      
    } 
    public function downloadExcelFile($type){
        $documents = Document::get()->toArray();
        return \Excel::create('expertphp_demo', function($excel) use ($documents) {
            $excel->sheet('sheet name', function($sheet) use ($documents)
            {
                $sheet->fromArray($documents);
            });
        })->download($type);
    }      
}
